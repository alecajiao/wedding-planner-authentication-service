# Wedding Planner Authentication Service

## PATCH LOGIN
### http://wedding-planner-authentication-service.wedding-planner-p1-cajiao.ue.r.appspot.com/users/login

## GET Verify User
### http://wedding-planner-authentication-service.wedding-planner-p1-cajiao.ue.r.appspot.com/users/:email/verify

## GET All Users
### http://wedding-planner-authentication-service.wedding-planner-p1-cajiao.ue.r.appspot.com/users

## POST
### http://wedding-planner-authentication-service.wedding-planner-p1-cajiao.ue.r.appspot.com/users
### Format
`{"email": "bilbo@email.com", "password": "123456", "fname": "bilbo", "lname": "baggins"}`
