import {Datastore} from '@google-cloud/datastore';
import express from 'express';
import cors from 'cors';

const app = express();
const PORT = process.env.PORT || 3006;
app.use(express.json()); 
app.use(cors());

const datastore = new Datastore();

app.post("/users", async (req, res)=>{
    const user = req.body;
    const key = datastore.key(["User", user.email]);
    const response = await datastore.save({key: key, data:user});
    console.log(response);
    res.status(201).send(`Successfully created user ${user.fname} ${user.lname}`);
});

app.get('/users', async (req, res)=>{
    const query  =  datastore.createQuery('User');
    const users = (await datastore.runQuery(query))[0];
    console.log(users);
    res.status(200).send(users);
});


// This endpoint will be used for our messaging service 
// to verify the user actually exist in the datastore
// We are not checking if they are authorized here
// GET /users/:email/verify
// 200 if user exists
// 404 if no user found
app.get("/users/:email/verify", async (req,res)=>{
    const email = req.params.email;
    const key = datastore.key(["User", email]);
    const user = (await datastore.get(key))[0];

    if (user){
        res.status(200).send("User is verified");
    }else{
        res.status(404).send("User not verified");
    } 
});

// This endpoint will be used to authorize users
// PATCH /users/login
// Request Body: {"email": "bill@wed.com", "password":"gatorfan1"}
// Response Body: {"fname":"Bill", "lname":"Smith"}
// We use patch to prevent returning sensitive user data (password)
app.patch("/users/login", async (req,res)=>{
    const credentials = req.body;
    const key = datastore.key(["User", credentials.email]);
    const user = (await datastore.get(key))[0];
    console.log(user);

    if (user.password === credentials.password){
        res.send({fname:user.fname, lname:user.lname});
    }else{
        res.status(401).send("Credentials did not match.")
    } 
});


app.listen(PORT, ()=>{console.log(`'Started server on port ${PORT}'`);});